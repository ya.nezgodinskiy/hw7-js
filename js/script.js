// 1. Опишіть своїми словами як працює метод forEach.
// Дозволяє запускати функцію для кожного елементу масиву

// 2. Як очистити масив?
// За допомогою length, Array.length = 0; Ця операція незворотна.

// 3. Як можна перевірити, що та чи інша змінна є масивом?
// За допомогою Array.isArray()

function filterBy(array, dataType) {
    return array.filter(item => typeof item !== dataType);
};
const array = ['hellow', 'world', 23, '23', null,];
const filteredArray = filterBy(array, 'string');
console.log(filteredArray);
